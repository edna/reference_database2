#!/bin/sh 

###############################################
# Script Name: build_marker_refdb.sh                                                                                         
# Description:
#   Convert crabs refdb into OBITools format
# Args:
#   - config_file : yaml configuration file
#   - crabs_db : crabs db file
# Requirements: obitools v.1.2.13
# Author:  Morgane BRUNO                                                
# Email: morgane.bruno@cefe.cnrs.fr
# Licence MIT
################################################

#_input
config=$1
crabs_db=$2


#_main
## Read configuration file
gb_v=$(grep -oP "(?<=midori_gb_v: ).*" $config)
db=$(grep -oP "(?<=midori_database: ).*" $config)
marker=$(grep -oP "(?<=marker_name: ).*" $config)
## Download taxdump
mkdir -p taxdump
wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz -P taxdump/
tar -zxvf taxdump/taxdump.tar.gz -C taxdump/
## Convert fasta into OBITools format
path="refdb/${db}/"
mkdir -p $path/obitools
obi_file="${path}CRABS_MIDORI2_GB${gb_v%_*}_${marker}_obi.fasta"
obi_db="${path}obitools/refdb_GB${gb_v%_*}_${marker}"
cut -d$'\t' -f1,3,11 $crabs_db | sed 's/^/>/; s/\t/ taxid=/; s/\t/;\n/' > $obi_file
obiconvert --skip-on-error --fasta -t taxdump --ecopcrdb-output=$obi_db $obi_file
