#!/bin/sh 

###############################################
# Script Name: build_marker_refdb.sh                                                                                         
# Description:
#   Download the Midori database and
#   Run a pcr in sillico with crabs to extract 
#   the marker of interest.
# Args:
#   - config_file : yaml configuration file
# Requirements: crabs v.1.0
# Author:  Morgane BRUNO                                                
# Email: morgane.bruno@cefe.cnrs.fr
# Licence MIT
################################################

color() {
    STARTCOLOR="\e[$2";
    ENDCOLOR="\e[0m";
    export "$1"="$STARTCOLOR%b$ENDCOLOR" 
}
color info 94m 

#_input
config=$1

#_main
## Read configuration file
midori_url=$(grep -oP "(?<=midori_link: ).*" $config)
gb_v=$(grep -oP "(?<=midori_gb_v: ).*" $config)
db=$(grep -oP "(?<=midori_database: ).*" $config)
primer_fw=$(grep -oP "(?<=primer_fw: ).*" $config)
primer_rv=$(grep -oP "(?<=primer_rv: ).*" $config)
max_mismatch=$(grep -oP "(?<=max_mismatch: ).*" $config)
min_len=$(grep -oP "(?<=min_length: ).*" $config)
max_len=$(grep -oP "(?<=max_length: ).*" $config)
marker=$(grep -oP "(?<=marker_name: ).*" $config)

## Download MIDORI database
path="refdb/${db}/"
mkdir -p $path
url="${midori_url}GenBank${gb_v}/RAW/total/MIDORI2_TOTAL_NUC_GB${gb_v%_*}_${db}_RAW.fasta.gz"
file_gz="${path}MIDORI2_TOTAL_NUC_GB${gb_v%_*}_${db}_RAW.fasta.gz"
file=${file_gz%.*}
if [ ! -f $file ]; then
  printf $info "::INFO:: Start download ${url} \n"
  wget $url -P $path
  gunzip $file_gz
fi

## Convert fasta into CRABS format
crabs_file="${path}CRABS_MIDORI2_GB${gb_v%_*}_${db}.txt"
marker_file="${path}CRABS_MIDORI2_GB${gb_v%_*}_${marker}.txt"
marker_filter="${path}CRABS_MIDORI2_GB${gb_v%_*}_${marker}_filtered.txt"

printf $info "::INFO:: Start CRABS in silico PCR ...\n"
printf $info "::INFO:: Primers: ${primer_fw} ${primer_rv} \n"
mkdir -p taxo
if [ ! -f taxo/names.dmp ]; then
  crabs --download-taxonomy --output taxo
fi
if [ ! -f $crabs_file ]; then
  crabs --import --import-format ncbi --input $file --names taxo/names.dmp --nodes taxo/nodes.dmp --acc2tax taxo/nucl_gb.accession2taxid --output $crabs_file  
fi
if [ ! -f $marker_file ]; then
  crabs --in-silico-pcr --input $crabs_file --output $marker_file --forward $primer_fw --reverse $primer_rv --mismatch $max_mismatch
fi
crabs --filter --input $marker_file --output $marker_filter --minimum-length $min_len --maximum-length $max_len --maximum-n 0 --rank-na 0 --no-species-id
