# Reference edna database creation

There are many tools available to perform in silico PCR:
- rCRUX
- CRUX
- CRABS 
- ECOPCR
- METACURATOR
- RESCRIPt 

I decided to test 3 of them and compare the results.

## Benchmark: rCRUX, CRABS, ECOPCR

| Tools | in silico PCR algorithm          |
|:------|:--------------------------------:|
| rCRUX | local alignment (BLAST)          |
| CRABS | semi-global alignment (cutadapt) |
| ECOPCR| agrep                            |

parameters:
- database: MARE-MAGE-DB
- error: 3

![](images/compare_n_sequence_crabs_rcrux_ecopcr.png)

**rCRUX:**
- pro: implemented in R
- cons: 
    - requires a local taxonomizr readable sqlite database (~80GB)
    - returns sequences with indels in the primer-binding regions

**CRABS:**
- pros:
    - well documented and easy to use
    - implements a function Pairwise Global Alignment to extract amplicon regions for which the reference sequence does not contain the full forward and reverse primer-binding regions.
- con: does not return a summary table with the number of mismatches in the primers

**ECOPCR (OBITools v.2):**
- pro: the results can be used directly by OBITools ecotag
- cons: 
    - no longer maintened
    - misses sequences where the primer-binding regions is incomplete

## Author

Morgane BRUNO, CEFE, morgane.bruno@cefe.cnrs.fr

## Licence

All code is licensed under the [MIT Licence](LICENSE)

